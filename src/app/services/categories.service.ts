import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': sessionStorage.getItem('access_token'),
    });
  }

  getDataPublic(url) {
    return this.http.get<any>(url);
  }

  getData(url) {
    return this.http.get<any>(url, {headers: this.headers});
  }

  getSubData(url) {
    return this.http.get<any>(url, {headers: this.headers});
  }

  postCreate(url, name, language, parent_id) {
    let params = {
      name,
      language,
      parent_id
    }
    return this.http.post<any>(url,  params, {headers: this.headers});
  }

  putUpdate(url, id, name, language, parent_id) {
    let params = {
      name,
      language,
      parent_id
    };
    return this.http.put<any>(url + '/' + id, params, {headers: this.headers});
  }

  getActive(url, id,){
    return this.http.get<any>(url + '/' + id, {headers: this.headers});
  }

  deleteDestroy(url, id,){
    return this.http.delete<any>(url + '/' + id, {headers: this.headers});
  }
}
