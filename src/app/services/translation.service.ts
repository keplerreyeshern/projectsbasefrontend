import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  headers:any;
  language:string;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': sessionStorage.getItem('access_token'),
    });
    this.getLang();
  }

  setLang(lang){
    this.http.get<any>(sessionStorage.getItem('url_global_api') + '/lang/' + lang, {headers: this.headers}).subscribe( response => {
      this.language = response;
    }, err => {
      if (err.status == 500) {
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if (err.status == 200) {
        // console.log(err.error);
        // console.log(err.error.text);
        this.language = err.error.text;
      } else {
        alert('se detecto un error comunicate con el administrador al obtener el idioma');
        console.log(err.status);
      }
    });
    return this.language;
  }

  getLang(){
    this.http.get<any>(sessionStorage.getItem('url_global_api') + '/lang', {headers: this.headers}).subscribe( response => {
      this.language = response;
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        // console.log(err.error);
        // console.log(err.error.text);
        this.language = err.error.text;
      } else {
        alert('se detecto un error comunicate con el administrador al obtener el idioma');
        console.log(err.status);
      }
    });
    return this.language;
  }
}

