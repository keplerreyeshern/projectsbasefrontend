import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {

  }

  postToken(params){
    return this.http.post<any>(sessionStorage.getItem('url_global') + '/oauth/token', params);
  }

  getDataUser(params){
    const headers = new HttpHeaders({
      'Authorization': params,
    });
    return this.http.get<any>(sessionStorage.getItem('url_global_api') + '/user', {headers});
  }
}
