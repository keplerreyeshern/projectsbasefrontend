import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule, HttpClient} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxSpinnerModule } from 'ngx-spinner';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './pages/layouts/admin-layout/admin-layout.component';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { AdminNavbarComponent } from './components/admin-navbar/admin-navbar.component';
import { AdminFooterComponent } from './components/admin-footer/admin-footer.component';
import { AdminSidebarComponent } from './components/admin-sidebar/admin-sidebar.component';
import { TranslationComponent } from './components/translation/translation.component';
import { AdminProductsComponent } from './pages/admin/admin-products/admin-products.component';
import { ProductsCreateComponent } from './pages/admin/admin-products/products-create/products-create.component';
import { ProductsEditComponent } from './pages/admin/admin-products/products-edit/products-edit.component';
import { AdminCategoriesComponent } from './pages/admin/admin-categories/admin-categories.component';
import { LoginComponent } from './pages/login/login.component';
import { PublicLayoutComponent } from './pages/layouts/public-layout/public-layout.component';
import { AdminSubCategoriesComponent } from './pages/admin/admin-categories/admin-sub-categories/admin-sub-categories.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    DashboardComponent,
    AdminNavbarComponent,
    AdminFooterComponent,
    AdminSidebarComponent,
    TranslationComponent,
    AdminProductsComponent,
    ProductsCreateComponent,
    ProductsEditComponent,
    AdminCategoriesComponent,
    LoginComponent,
    PublicLayoutComponent,
    AdminSubCategoriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
