import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminLayoutComponent} from "./pages/layouts/admin-layout/admin-layout.component";
import {DashboardComponent} from "./pages/admin/dashboard/dashboard.component";
import {AdminProductsComponent} from "./pages/admin/admin-products/admin-products.component";
import {ProductsCreateComponent} from "./pages/admin/admin-products/products-create/products-create.component";
import {ProductsEditComponent} from "./pages/admin/admin-products/products-edit/products-edit.component";
import {LoginComponent} from "./pages/login/login.component";
import {PublicLayoutComponent} from "./pages/layouts/public-layout/public-layout.component";
import {AdminCategoriesComponent} from "./pages/admin/admin-categories/admin-categories.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: PublicLayoutComponent },
  {
    path:'admin',
    component: AdminLayoutComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'products',
        component: AdminProductsComponent,
        children: [
          { path: '', redirectTo: '', pathMatch: 'full' },
          { path: 'create', component: ProductsCreateComponent },
          { path: 'edit/:id', component: ProductsEditComponent },
        ]
      },
      {
        path: 'categories',
        component: AdminCategoriesComponent,
        children: [
          { path: '', redirectTo: '', pathMatch: 'full' },
          { path: 'create', component: ProductsCreateComponent },
          { path: 'edit/:id', component: ProductsEditComponent },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
