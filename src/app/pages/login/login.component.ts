import { Component, OnInit } from '@angular/core';
import {LoginService} from "../../services/login.service";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  storage:string;
  url:string;
  access_token:string;
  user:any;
  company:any;

  constructor(private authService: LoginService,
              private router: Router,
              private spinnerService: NgxSpinnerService) {
    if (sessionStorage.getItem('access_token')){
      this.router.navigate(['admin']);
    }
  }

  ngOnInit(): void {
    this.url = sessionStorage.getItem('url_global');
  }

  submit(form: NgForm){
    this.spinnerService.show();
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'Hut1JrybPSKVBD6nmhZ9D5y8BVNErtXDEe5buqZj',
      username: form.value.email,
      password: form.value.password,
    };
    console.log(form.value);
    console.log(form);
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      this.spinnerService.hide();
      sessionStorage.setItem('token', JSON.stringify(response));
      this.storage = sessionStorage.getItem('token');
      let start = this.storage.indexOf('access_token', 0);
      let substr = this.storage.substring(start);
      start = substr.indexOf(':', 0) + 2;
      substr = substr.substring(start);
      const end = substr.indexOf('"', 0);
      this.access_token = substr.substring(0, end );
      sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
      console.log(sessionStorage.getItem('access_token'));
      this.router.navigate(['/admin']);
    }, err => {
      if(err.status == 400){
        this.spinnerService.hide();
        alert('Las credeciales no coinciden con la base de datos verifica y vuelve a intentar');
      } else if(err.status == 500){
        this.spinnerService.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 404){
        this.spinnerService.hide();
        alert('¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediaro con el administrador');
      } else {
        this.spinnerService.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }
}
