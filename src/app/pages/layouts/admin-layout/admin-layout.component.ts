import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginService} from "../../../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.sass']
})
export class AdminLayoutComponent implements OnInit {

  url:string;
  access_token:string;
  user:any;
  language = sessionStorage.getItem('language');

  constructor(private authService: LoginService,
              private router: Router) { }

  ngOnInit(): void {
    this.getData(sessionStorage.getItem('access_token'));
    if (!sessionStorage.getItem('access_token')){
      this.router.navigate(['/login']);
    }
    setTimeout(() => {
      if( this.user.email_verified_at == null){
        this.router.navigate(['/verify-email']);
      }
    }, 4000);
  }

  getData(params){
    this.authService.getDataUser(params).subscribe( response => {
      this.user = response;
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
        console.log(err.status);
      }
    });
  }

  getLang(lang){
    this.language = lang;
  }

}
