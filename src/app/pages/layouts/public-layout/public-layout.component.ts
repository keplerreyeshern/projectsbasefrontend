import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-public-layout',
  templateUrl: './public-layout.component.html',
  styleUrls: ['./public-layout.component.sass']
})
export class PublicLayoutComponent implements OnInit {

  constructor() {
    sessionStorage.setItem('url_global', 'http://projectsbase.test');
    sessionStorage.setItem('url_global_api', sessionStorage.getItem('url_global') + '/api');
    sessionStorage.setItem('url_images', sessionStorage.getItem('url_global') + '/storage/images');
    sessionStorage.setItem('language' , 'es');
  }

  ngOnInit(): void {
  }

}
