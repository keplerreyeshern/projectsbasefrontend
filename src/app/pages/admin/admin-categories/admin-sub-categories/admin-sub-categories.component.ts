import {Component, OnInit, forwardRef, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {CategoriesService} from "../../../../services/categories.service";
import { faPlus, faImages, faTrashAlt, faPowerOff, faBan, faSave } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-admin-sub-categories',
  templateUrl: './admin-sub-categories.component.html',
  styleUrls: ['./admin-sub-categories.component.sass']
})
export class AdminSubCategoriesComponent implements OnInit {

  @Input() parent_id:string;
  @Input() language:string;
  @ViewChild(AdminSubCategoriesComponent) SubCategories: AdminSubCategoriesComponent;

  faPlus = faPlus;
  faImages = faImages;
  faTrashAlt = faTrashAlt;
  faPowerOff = faPowerOff;
  faBan = faBan;
  faSave = faSave;
  faEdit = faEdit;
  categoriesS:any[]=[];
  url = sessionStorage.getItem('url_global_api') + '/categories';
  images = sessionStorage.getItem('url_images');
  idNew = 0;
  idEdit = 0;
  name = '';


  constructor(private loading: NgxSpinnerService,
              private service: CategoriesService) { }

  ngOnInit(): void {
    this.getData();
  }

  public getData(){
    this.service.getSubData(this.url + '/create').subscribe( response =>{
      this.categoriesS = response.filter(item => item.parent_id == this.parent_id);
    }, error => {
      if(error.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
  }

  public plus(CategoryName, parent_id){
    this.loading.show();
    this.service.postCreate(this.url, CategoryName, this.language, parent_id).subscribe( response =>{
      this.categoriesS.push(response);
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
  }

  public subPlus(category){
    if(this.idNew == 0){
      this.idNew = category.id;
      this.name = '';
    } else {
      this.idNew = 0;
      this.name = '';
    }
  }

  public saveSubPlus(parent_id){
    this.SubCategories.plus(this.name, parent_id);
    this.idNew = 0;
    this.name = '';
  }

  public edit(category){
    this.idEdit = category.id;
    if (this.language == 'es'){
      this.name = category.name.es;
    } else if(this.language == 'en'){
      this.name = category.name.en;
    }
  }

  public active(category){
    this.loading.show();
    this.service.getActive(this.url, category.id).subscribe( response => {
      const index = this.categoriesS.findIndex(item => item.id == category.id);
      this.categoriesS[index].active = response.active;
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
  }

  public delete(category){
    let name = '';
    if(this.language == 'es'){
      name = category.name.es;
    } else if(this.language == 'en'){
      name = category.name.en;
    }
    const result = confirm('¿Esta seguro que decea eliminar la categoria ' + name +'?');
    if(result){
      this.service.deleteDestroy(this.url, category.id).subscribe( response =>{
        const index = this.categoriesS.findIndex(item => item.id == category.id);
        this.categoriesS.splice(index, 1);
        this.loading.hide();
      }, error => {
        if(error.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
          console.log(error.status);
        }
      });
    }
  }

  public canceled(){
    this.idEdit = 0;
    this.name = '';
    this.idNew = 0;
  }

  public update(category){
    this.loading.show();
    this.service.putUpdate(this.url, category.id, this.name, this.language, null).subscribe( response =>{
      const index = this.categoriesS.findIndex(item => item.id == category.id);
      this.categoriesS[index] = response;
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
    this.idEdit = 0;
    this.name = '';
  }

}
