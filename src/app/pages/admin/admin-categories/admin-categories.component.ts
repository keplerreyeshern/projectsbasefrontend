import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { faPlus, faWarehouse, faImages, faTrashAlt, faPowerOff, faBan, faSave } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import {NgxSpinnerService} from "ngx-spinner";
import {Router} from "@angular/router";
import {CategoriesService} from "../../../services/categories.service";
import {AdminSubCategoriesComponent} from "./admin-sub-categories/admin-sub-categories.component";

@Component({
  selector: 'app-admin-categories',
  templateUrl: './admin-categories.component.html',
  styleUrls: ['./admin-categories.component.sass']
})
export class AdminCategoriesComponent implements OnInit {

  @ViewChild(AdminSubCategoriesComponent) SubCategories: AdminSubCategoriesComponent;

  faWarehouse = faWarehouse;
  faPlus = faPlus;
  faImages = faImages;
  faTrashAlt = faTrashAlt;
  faPowerOff = faPowerOff;
  faBan = faBan;
  faSave = faSave;
  faEdit = faEdit;
  url = sessionStorage.getItem('url_global_api') + '/categories';
  images = sessionStorage.getItem('url_images');
  categories:any[]=[];
  language =  sessionStorage.getItem('language');
  idEdit = 0 ;
  name:any;
  idNew = 0;
  new = false;
  page:number;
  flag:string;

  constructor(private loading: NgxSpinnerService,
              private service: CategoriesService,
              private router: Router) { }

  ngOnInit(): void {
    this.getData();
  }

  public getData(){
    this.loading.show();
    this.service.getData(this.url).subscribe( response =>{
      this.categories = response;
      if(this.language == 'es'){
        for(let i=0; i<this.categories.length; i++){
          this.categories[i].nameTrans = this.categories[i].name.es;
          this.flag = 'assets/images/flags/Flag_of_mexico.png';
        }
      } else if(this.language == 'en'){
        for(let i=0; i<this.categories.length; i++){
          this.categories[i].nameTrans = this.categories[i].name.en;
          this.flag = 'assets/images/flags/Flag_of_United.svg';
        }
      }
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
  }

  public plus(){
    if(this.new){
      this.new = false;
    } else {
      this.new = true;
    }
  }

  public save(){
    this.loading.show();
    this.service.postCreate(this.url, this.name, this.language, null).subscribe( response =>{
      this.categories.push(response);
      const index = this.categories.findIndex(item => item.id == response.id);
      this.categories[index].nameTrans = this.name;
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
    this.name = '';
    this.new = false;
  }

  public subPlus(category){
    if(this.idNew == 0){
      this.idNew = category.id;
      this.name = '';
    } else {
      this.idNew = 0;
      this.name = '';
    }
  }

  public saveSubPlus(parent_id){
    this.SubCategories.plus(this.name, parent_id);
    this.idNew = 0;
    this.name = '';
  }

  public edit(category){
    if(this.idEdit == 0){
      this.idEdit = category.id;
      this.name = category.nameTrans;
    } else {
      this.idEdit = 0;
      this.name = '';
    }
  }

  public active(category){
    this.loading.show();
    this.service.getActive(this.url, category.id).subscribe( response => {
      const index = this.categories.findIndex(item => item.id == category.id);
      this.categories[index].active = response.active;
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
  }

  public delete(category){
    let name = '';
    if(this.language == 'es'){
      name = category.name.es;
    } else if(this.language == 'en'){
      name = category.name.en;
    }
    const result = confirm('¿Esta seguro que decea eliminar la categoria ' + name +'?');
    if(result){
      this.service.deleteDestroy(this.url, category.id).subscribe( response =>{
        const index = this.categories.findIndex(item => item.id == category.id);
        this.categories.splice(index, 1);
        this.loading.hide();
      }, error => {
        if(error.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
          console.log(error.status);
        }
      });
    }
  }

  public canceled(){
    this.idEdit = 0;
    this.name = '';
    this.idNew = 0;
    this.new = false;
  }

  public update(category){
    this.loading.show();
    const index = this.categories.findIndex(item => item.id == category.id);
    this.categories[index].nameTrans = this.name;
    this.service.putUpdate(this.url, category.id, this.name, this.language, null).subscribe( response =>{
      this.categories[index] = response;
      this.loading.hide();
    }, error => {
      if(error.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
        console.log(error.status);
      }
    });
    this.idEdit = 0;
    this.name = '';
  }

  getLang(lang){
    this.language = lang;
    if(lang == 'es'){
      for(let i=0; i<this.categories.length; i++){
        this.categories[i].nameTrans = this.categories[i].name.es;
        this.flag = 'assets/images/flags/Flag_of_mexico.png';
      }
    } else if(lang == 'en'){
      for(let i=0; i<this.categories.length; i++){
        this.categories[i].nameTrans = this.categories[i].name.en;
        this.flag = 'assets/images/flags/Flag_of_United.svg';
      }
    }
  }

}
