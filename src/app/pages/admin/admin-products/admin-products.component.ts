import { Component, OnInit } from '@angular/core';
import { faPlus, faStore, faImages } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.sass']
})
export class AdminProductsComponent implements OnInit {

  faPlus = faPlus;
  faStore = faStore;
  faImages = faImages;

  constructor() { }

  ngOnInit(): void {
  }

}
