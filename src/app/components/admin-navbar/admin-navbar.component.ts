import { Component, OnInit } from '@angular/core';
import {LoginService} from "../../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.sass']
})
export class AdminNavbarComponent implements OnInit {

  url:string;
  access_token:string;
  user = {
    name: 'Anonimo',
  };

  constructor(private authService: LoginService,
              private router: Router) { }

  ngOnInit(): void {
    this.getData(sessionStorage.getItem('access_token'));
  }

  getData(params){
    this.authService.getDataUser(params).subscribe( response => {
      this.user = response;
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
