import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faHome, faStore, faWarehouse, faUser, faImages } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.sass']
})
export class AdminSidebarComponent implements OnInit {

  @Output() selectLangS: EventEmitter<string>;

  faHome = faHome;
  faStore = faStore;
  faWarehouse = faWarehouse;
  faUser = faUser;
  faImages = faImages;

  constructor() {
    this.selectLangS = new EventEmitter();
  }

  ngOnInit(): void {
  }

  getLang(lang){
    this.selectLangS.emit(lang);
  }

}
