import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslationService} from "../../services/translation.service";
import {TranslateService} from "@ngx-translate/core";
import { faLanguage } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.sass']
})
export class TranslationComponent implements OnInit {

  faLanguage = faLanguage;
  activeLang = sessionStorage.getItem('language');

  @Output() selectLang: EventEmitter<string>;

  constructor(private translate: TranslateService,
              private service: TranslationService) {
    this.service.setLang(this.activeLang);
    this.translate.setDefaultLang(this.activeLang);
    this.selectLang = new EventEmitter();
  }

  ngOnInit(): void {
  }

  public changeLenguage(lang){
    this.service.setLang(lang);
    this.activeLang = lang;
    this.translate.use(lang);
    sessionStorage.setItem('language', lang);
    this.selectLang.emit(lang);
  }

}
